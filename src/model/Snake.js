"use strict";

const GObject = require("./GObject");
const Vector = require("./Vector");
const Food = require("./Food");

class Snake extends GObject {

    constructor(position, orientation) {
        super(position);

        this.segments = [position];
        this.lastRemovedSegment = null;
        this.orientation = orientation || Vector.Zero;
    }

    get movable() {
        return true;
    }

    get colidable() {
        return true;
    }

    get controllable() {
        return true;
    }

    get size() {
        return this.segments.length;
    }

    get head() {
        return this.segments[0];
    }

    move(terrain) {
        if (this.orientation.x !== this.orientation.y) {
            let newHeadPosition = terrain.projectVectorToTerrain(this.head.addVector(this.orientation));
            if (this.isOccupied(newHeadPosition)) {
                this.emitCollision(this);
            } else {
                this.segments.unshift(newHeadPosition);
                this.lastRemovedSegment = this.segments.pop();
            }
        }
    }

    isOccupied(vector) {
        for (let i = 0; i < this.segments.length; i++) {
            if (this.segments[i].isEqual(vector)) {
                return true;
            }
        }
        return false;
    }

    eat(food) {
        if (!food instanceof Food) {
            throw new Error(`Food GObject expected: ${typeof food}`);
        }
        this.segments.push(this.lastRemovedSegment);
    }

    extendHead(segment) {
        this.segments.unshift(segment);
    }

    changeOrientation(direction) {
        this.orientation = Vector.getDirectionVector(direction);
    }

}

module.exports = Snake;

