"use strict";

const GObjectGenerator = require("./GObjectGenerator");
const Vector = require("../model/Vector");
const Obstacle = require("../model/Obstacle");

class ObstacleGenerator extends GObjectGenerator {

    generateInitialState() {
        // this.terrain.clear('obstacles');
        this.options.forEach((opts) => this.generateByPattern(opts));
    }

    generateByPattern(opts) {
        let startPosition = new Vector(opts.start_position);
        let orientation = new Vector(opts.orientation);
        for (let i = 0; i < opts.length; i++) {
            let obstaclePosition = startPosition.addVector(orientation.multiplyWithScalar(i));
            this.generate({position: obstaclePosition});
        }
    }

    generate(opts) {
        // Now the signature of the base class makes sense again
        return this.terrain.add(new Obstacle(opts.position));
    }

}

module.exports = ObstacleGenerator;