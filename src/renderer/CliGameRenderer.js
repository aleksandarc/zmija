"use strict";

const GameRenderer = require("./GameRenderer");
const CliGameCanvas = require("../renderer/canvas/CliGameCanvas");

/**
 *Renderer renders the terrain and all gObjects inside of it.
 */
class CliGameRenderer extends GameRenderer {

    constructor(terrain, options) {
        super(terrain, options);
        this.canvas = new CliGameCanvas(terrain);
    }


    render(terrain) {
        for (let [k, gObject] of Object.entries(terrain.all)) {
            let renderer = this.loadRenderer(gObject);
            renderer.render(gObject, this.canvas);
        }
        console.log(this.canvas.canvasToStr());
    }

    clearScreen() {
        console.clear();
        this.canvas.clear();
    }

    renderScore(player) {
        console.log(`Player ${player.name}: ${player.score}`);
    }

    renderMenu() {
        // Since in CLI mode the UX incorporates both "rendering" the input question and reading the input
        // We can either let the Input controller handle the load
    }
}


module.exports = CliGameRenderer;

