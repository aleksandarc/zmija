#GInkoSnake


##Executive Summary

We will offer a nostalgic game that we all enjoyed in the previous century with the intention 
of learning about design patterns, project development cycle, archictecture and meaning of life.


##Business​ ​Objectives

Business goal of this project is making of 2D GSnake played in CLI or browser.
Game is played in 2D terrain and consists of player controlling the snake while avoiding obstacles and eating food.
During the process of developing this game we will utilize model-renderer-generator approach along with en event loop. 
Player will be able to configure a game by editing a configuration file.
Estimated deadline for completion of this project is by the end of the year.

<!-- ##Background -->

##Scope
Game will consists of a single snake played in 2D environment. 
Score will be displayed but not saved.
The terrain and obstacles are fixed and they are contained in the configuration file.
We will not provide game updates after the agreed deadline.

##Features
Players will be able to:
* maneuver around obstacles 
* extend their snake, its speed and increase the score by eating food
* and enjoy having a challenging experience

##Functional​ ​requirements

Game World:
* The game will be implemented with NodeJS
* Abstract entities
  * Field is smallest segment of the world and is defined by its position on x and y-axes.
  * Any object in the game can move according to the following orientation
    * N: (0, -1);  
    * E: (+1, 0);  
    * S: (0, +1);  
    * W: (-1, 0); 
  * Renderer renders the terrain and all GObjects inside of it.
  * Generator has the purpose of initializing a GObject.
* GObject is an abstraction of a game object that resides in the game world.
  * It has a position within the game world expressed in X and Y coordinates
  * It can occupy a discrete space measurable in fields (game space units);
  * The minimum size of a GObject is 1, occupying 1 field in the game world.
  * It can be movable (or fixed).
    * Movement is defined by :
      * Orientation
      * Speed
  * It can be collideable
      * It has the ability to check for collision with other GObjects and decide on a collision resolution action.  
* Terrain is a configurable rectangle shaped 2D matrix of GObject size spaces (fields). 
  * It contains all of the GObjects
* Obstacle is a type of collideable fixed GObject. 
  * It is linear-shaped, having a length and orientation (starting position, and direction)
  * It is loaded from the configuration file or it is generated at random during the startup process.
  * Any collision with an obstacle leads to the end of the game.
* Food is a type of collideable fixed GObject.
  * It has the size of 1 field.
  * A collision with:
    * Wall results in:
      * Re-spawning of the food
    * Snake Head results in:
      * the food being eaten;
* Snake is a collideable movable GObject. 
  * It has a Head.
  * It has a configurable initial size greater than 1.
  * It is spawned randomly at the beginning of the game.
  * It occupies the position defined by an array of edge connected fields (No diagonal linking)
  * It moves across the world Terrain in increments of 1 field in a set orientation
    * The orientation is persisted between moves.
    * The orientation is set by the Player
    * The initial speed of the snake is configurable 
  * A collision of the Head with:
    * Wall or a Snake results in:
      * Ending the game
    * Food results in:
      * the food being eaten by the snake;
  * When the snake eats a food
    * the snake increases its length by 1 by adding a segment where the food has been;
    * a new Food item to be re-spawned;
    * the snake speed increases by certain percent.
* Player is the end user of this game who is controlling the snake with the keyboard.
  * Controls are configurable
  * Controls defines:
    * Orientation of the Snake movement
    * Pausing of the game
    * Exit of the game
    * Re-start the game
  * Player goals:
    * Collecting points
    * Avoiding collision with:
      * snake body
      * obstacle
* Game engine
  * Game loop consist of update and render methods which are called periodically depending on FPS
    * Update updates all the game models and check collision between them 
    * Render call Renderer to render game content

##Personnel​ ​requirements
5 GInkubator developers + 1 Architect will be working on this project.

##Reporting​ ​and​ ​quality​ ​assurance

We will approach reporting by having a list of functionalities and checking them one by one:
* Terrain generation, with configurable or default parameters
* Obstacle generation, with configurable or default parameters
* Snake generation, with configurable or default parameters
* Food generation with random position inside of terrain
* Game Loop, Snake movement, collision with obstacle and/or food and snake growth
* Porting from CLI to browser playable game


##Delivery​ ​schedule
Deadline is 2017-12-29T15:00:00Z
Milestones are:
* 1st week   =>Organizing teams,agreeing on a model case and implementing interfaces.
* 2nd week   =>Implementing world ,obstacles, snake.
* 3th week   =>Integrating snake into existing world and managing its movements.
* 4th week   =>Testing and bug fixing.
##Other​ ​requirements

##Assumptions
Players will enjoy a nostalgic feeling of playing an old school game.

##Limitations
Lack of Enterprise-grade equipment. 

##Risks
We will mitigate this risk of delays caused by computer failure by making frequent commits to our git repository.
Relaying on a library that we haven't analyzed.

##Appendix​ ​A​ ​–​ ​Glossary​ ​of​ ​Terms 
Player - end user of this product and a person playing this game
Snake - an item in the terrain that player is controlling with a keyboard keys
Obstacle - an item in the terrain that snake can't go through
Food - an item in the terrain that increases the score and snake length when its eaten
Score - number of times that food is eaten multiplied by food value
