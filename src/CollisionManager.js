"use strict";

const Food = require("./model/Food");
const Snake = require("./model/Snake");
const Obstacle = require("./model/Obstacle");
const Vector = require("./model/Vector");

class CollisionManager {

    handleCollision(engine, evt) {
        switch (true) {
            case evt.from instanceof Snake:
                switch (true) {
                    case evt.with instanceof Food:
                        return this.onSnakeWithFood(engine, evt.from, evt.with);
                    case evt.with instanceof Snake:


                        return this.onSnakeWithSnake(engine, evt.from, evt.with);
                    case evt.with instanceof Obstacle:

                        return this.onSnakeWithObstacle(engine, evt.from, evt.with);
                    default:
                        throw new Error(`Unsupported gObject: ${typeof evt.with}`)
                }
                return;
            default:
                throw new Error(`Unsupported gObject: ${typeof evt.from}` + evt.from)
        }

    }

    onSnakeWithFood(engine, snake, food) {
        snake.eat(food);
        // HACK: Let's not recreate the food but reuse it.
        engine.terrain.relocate(food);
        engine.playerManager.getPlayerByHisGObject(snake).scoreIncrement();
        engine.score += 10;
    }

    onSnakeWithSnake(engine, theHitter, theVictim) {
        this._die(engine, theHitter, theVictim);
    }

    onSnakeWithObstacle(engine, snake, obstacle) {
        this._die(engine, snake, obstacle);
    }

    _die(engine, theHitter, theVictim) {
        engine.stop(theHitter.head);
    }


}

module.exports = CollisionManager;