"use strict";

const Vector = require("./Vector");
const GObject = require("./GObject");
const GObjectStorage = require("./GObjectStorage");

const events = require("events");

class Terrain extends events.EventEmitter {

    constructor(opts) {
        super();
        this.width = opts.width;
        this.height = opts.height;
        // This is the GameObject storage
        this.storage = new GObjectStorage();
        this.max_tries = opts.max_tries || 0x64;
    }

    getRandomPosition(unoccupied) {
        if (unoccupied) {
            unoccupied = typeof unoccupied === "number" ? unoccupied : this.max_tries;
            let position, tries = unoccupied;
            do {
                position = this.getRandomPosition(false);
            } while (this.isOccupied(position) && tries--);
            if (tries) {
                return position;
            }
            throw new Error(`Max tries for getting a random position exceeded: ${unoccupied}`);
        }
        return new Vector(
            Math.floor(Math.random() * this.width),
            Math.floor(Math.random() * this.height)
        );
    }

    projectVectorToTerrain(vector) {

        // TODO: Extend the algorithm to cover all use cases
        if (vector.y < 0) {
            vector.y = this.height - 1;
        } else if (vector.x < 0) {
            vector.x = this.width - 1;
        } else {
            vector.y %= this.height;
            vector.x %= this.width;

        }
        return vector;
    }

    getCenterPosition() {
        return new Vector(
            Math.floor(this.width / 2),
            Math.floor(this.height / 2)
        );
    }

    getRandomNeighbour(position, excluded) {
        if (!position instanceof Vector || !excluded instanceof Vector) {
            throw new Error("Not a Vector!!!");
        }
        return this.projectVectorToTerrain(position.addVector(Vector.randomOrientation(excluded, Vector.ORTOGONAL_ORIENTATIONS)));
    }

    add(gObject) {
        if (!gObject instanceof GObject) {
            throw new Error(`GObject expected: ${typeof gObject}`);
        }
        gObject.on("collision", (evt) => {
            this.emit("collision", evt);
        });
        return this.storage.add(gObject);
    }


    /**
     * Removes a gObject from the terrain by ID
     * @param {string} gObjectID
     * @returns {GObject|undefined}
     */
    remove(gObjectID) {
        return this.storage.removeByID(gObjectID);
    }

    /**
     * Removes all GObject from the terrain
     */
    clear() {
        return this.storage.clear();
    }

    relocate(gObject) {
        gObject.position = this.getRandomPosition();
    }

    get movable() {
        return this.storage.movable;
    }

    get collidable() {
        return this.storage.collidable;
    }

    get controllable() {
        return this.storage.controllable;
    }

    get endGameActive() {
        return this.storage.endGameActive;
    }

    get all() {
        return this.storage.gObjects;
    }

    isOccupied(vector) {
        for (let id in this.storage.collidable) {
            if (this.storage.collidable[id].isOccupied(vector)) {
                return true;
            }
        }
        return false;
    }


}

module.exports = Terrain;