"use strict"

const GObject = require("./GObject");

class Obstacle extends GObject {

    constructor(position) {
        super(position);
    }

    get colidable() {
        return true;
    }

}

module.exports = Obstacle;