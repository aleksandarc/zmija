"use strict";

const Terrain = require("./model/Terrain");
const PlayerManager = require("./PlayersManager");
const CollisionManager = require("./CollisionManager");

const GameRenderer = require("./renderer/GameRenderer");

const InputController = require("./controller/InputController");

const WorldGenerator = require("./generator/WorldGenerator");
const PlayerGenerator = require("./generator/PlayerGenerator");

const GameController = require("./controller/play/GameController");
const PlayerController = require("./controller/play/PlayerController");

class GameEngine {

    constructor(config, done) {
        this.config = config;

        this.render_config = this.config.Renderer;

        this._quit = done || function () {
        };

        this.playerManager = new PlayerManager();

        this.playerGenerator = new PlayerGenerator(this.playerManager);

        this.collisionManager = new CollisionManager();

        this.terrain = new Terrain(this.config.Terrain);
        this.terrain.on("error", this.quit.bind(this));
        this.terrain.on("collision", (evt) => {
            this.collisionManager.handleCollision(this, evt)
        });

        this.worldGenerator = new WorldGenerator(this.terrain, this.config.Generator);

        // TODO: How do we know if we're using a CLI or WEB Renderer)? Adapter Pattern
        /**
         * Check if game is running inside Cli or Web
         */

        this.gameRenderer = GameRenderer.factory(this.isCLI(), this.terrain, this.render_config);


        // This is used to separate the CLI and DOM issue in a clean way
        // It handles all the input going into the game whether CLI or DOM
        this._inputController = InputController.factory(this.isCLI());

        this.gameController = new GameController();
        this.gameController.on("pause", () => {
            this.pause();
        });
        this.gameController.on("restart", () => {
            this.restart();
        });
        this.gameController.on("quit", () => {
            this.quit();
        });

        this.gameController.on("startMenuProcess", this._startMainMenu.bind(this));
        this.gameController.on("completeMenuProcess", this._completeMenuProcess.bind(this));
        this.gameController.on("error", console.error);

        // Here we hook the input controller onto the game controller
        this._inputController.on("raw_key", this.gameController.onRawKey.bind(this.gameController));

        this._inputController.on("loadPlayerCount", this.gameController.onLoadPlayerCount.bind(this.gameController));
        this._inputController.on("loadPlayerNames", this.gameController.onLoadPlayerNames.bind(this.gameController));
        this._inputController.on("error", (error) => {
            throw new Error(error);
        });

        // Engine FSM
        this._interval = undefined;
        this._started = undefined;
        this._endGameInterval = undefined;


        // This is where we setup the players, and only the players.
        // The world will be reset on _init (Just read the comment of the method)
        //this.playerGenerator.generateInitialState(this.config.Generator.Players);
    }

    /**
     * This is where the GameEngine builds the world.
     */
    _init(players) {
        // We create the world without control
        this.worldGenerator.generateInitialState();
        // And now we generate the characters that can have control

        //CURRENTLY WORKS FOR ONLY 2 PLAYERS UNTILL CONFIG IS REDONE!
        this.playerGenerator.generateInitialState(players);

        this.playerManager.players.forEach((player, idx) => {
            let opts = this.config.Players[idx];
            player.character = this.worldGenerator.generateCharacter(this.config.Generator.Snakes[idx]);
            player.resetScore();
            // And here we create and link the player controller
            let controls = new PlayerController(opts.controls);
            // And here we link it to the input controller so that we can receive signals
            this._inputController.on("raw_key", controls.onRawKey.bind(controls));
            // And here we decide on how to react to different signals
            controls.on("changeOrientation", (orientation, snakeID) => {
                // And here is the hook back to the model
                player.character.changeOrientation(orientation);
            });
        });
        return Date.now();
    }

    // TODO@wiz: Shouldn't this be private.
    update() {
        for (let [moverID, movingGObject] of Object.entries(this.terrain.movable)) {
            movingGObject.move(this.terrain);
            for (let [id, gObject] of Object.entries(this.terrain.collidable)) {
                if (!movingGObject.isEqual(gObject) && movingGObject.colidable) {
                    if (movingGObject.hasCollided(gObject)) {
                        movingGObject.emitCollision(gObject);
                    }
                }
            }
        }
    }

    /**
     * This method will be used instead of update after game ends.
     * @private
     */
    _endGameUpdate() {
        for (let [activeID, activeGObject] of Object.entries(this.terrain.endGameActive)) {
            activeGObject.move(this.terrain);
        }
    }

    // TODO@wiz: Shouldn't this be private.
    draw() {
        this.gameRenderer.renderWorld(this.terrain);
        this.gameRenderer.renderGUI(this.playerManager);
    }

    isWeb() {
        let isBrowser = new Function("try {return this===window;}catch(e){ return false;}");
        return isBrowser();
    }

    isCLI() {
        let isNode = new Function("try {return this===global;}catch(e){return false;}");
        return !!isNode();
    }

    get isRunning() {
        return !!this._interval;
    }

    get hasStarted() {
        return !!this._started;
    }

    boot() {
        this.mainMenu();
        //this.start();
    }

    _startMainMenu(playerCount) {
        this.gameRenderer.renderPlayerInput(playerCount)
        // TODO@ion: See how to hook to the InputController if necessary
    }

    _completeMenuProcess(players) {
        this.start(players);
    }

    mainMenu() {
        this.gameRenderer.renderMenu();
        this._inputController.startMenu()
        // TODO@ion: See how to hook to the InputController if necessary
    }

    start(players) {
        if (this.isRunning) {
            return false; // Or throw an error
        }
        this._started = this._init(players);
        this._unpause();

    }

    _unpause() {
        this._interval = setInterval(this._run.bind(this), 1000 / this.config.FPS);
    }

    _run() {
        this.update();
        this.draw();
    }

    restart() {
        this.stop();
        this.start();
        this._endGameInterval = clearInterval(this._endGameInterval);
    }

    pause() {
        if (this.hasStarted) {
            if (this.isRunning) {
                this._interval = clearInterval(this._interval);
                setTimeout(() => {
                    console.log("PAUSE");
                });
            } else {
                this._unpause();
            }
        }
    }

    /**
     * This is used for end game animations.
     * @private
     */
    _endGameAnimating(position) {
        this.worldGenerator.generateParticles(position);
        this._endGameInterval = setInterval(() => {
            this._endGameUpdate();
            this.draw();
        }, 1000 / this.config.FPS);
        setTimeout(() => {
            this._endGameInterval = clearInterval(this._endGameInterval);
        }, 5000);
    }

    stop(position) {
        if (this.hasStarted) {
            if (this.isRunning) {
                this._interval = clearInterval(this._interval);
                this._started = undefined;
                setTimeout(() => {
                    console.log("Game Over!");
                });
                //start end game animations
                if (position) {
                    this._endGameAnimating(position);
                }
            }
        }
        // Or throw an error
    }

    quit(err) {
        if (err) {
            console.error(err);
        }
        this._quit();
    }


}

module.exports = GameEngine;