"use strict";

class GObjectGenerator {

    constructor(terrain, options) {
        this._terrain = terrain;
        this.options = options;
        // TODO@wiz: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperties
        // This should be used in all constructors
    }

    /**
     *
     * @returns {Terrain}
     */
    get terrain() {
        return this._terrain;
    }

    /**
     * This method generate multiple GObject or one complex GObject and call (generate) for it
     */
    generateInitialState() {

    }

    /**
     * This method generate one instance or one segment of GObject and put it in GObjectStorage}
     * @param {*} [opts]
     * @return {GObject}
     */
    generate(opts) {

    }


}

module.exports = GObjectGenerator;