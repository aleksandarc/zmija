"use strict"

const GObject = require("../model/GObject");

class Renderer {

    constructor(config) {
        this.config = config;
    }


    /**
     * Renders GObject to canvas
     * @param gObject - GObject for render
     * @param canvas
     */
    render(gObject, canvas) {
        if (!gObject instanceof GObject) {
            throw new Error(`Unsupported object. Can't be rendered: ${typeof gObject}`);
        }
    }

}

module.exports = Renderer;