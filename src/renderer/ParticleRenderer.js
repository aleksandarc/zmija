"use strict";

const Renderer = require("./Renderer");
const Particle = require("../model/Particle");

class ParticleRenderer extends Renderer {

    /**
     * This method renders single particle.
     */
    render(particle, canvas) {
        if (!(particle instanceof Particle)) {
            throw new Error("Must be particle");
        }
        canvas.drawSegment(particle.position, this.config.particle);
    }

}

module.exports = ParticleRenderer;