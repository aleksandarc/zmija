"use strict";

const GObject = require("./GObject");

class Food extends GObject {

    constructor(position) {
        super(position);
    }

    get colidable() {
        return true;
    }

}

module.exports = Food;