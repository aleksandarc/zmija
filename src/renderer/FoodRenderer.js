"use strict"

const Renderer = require("../renderer/Renderer");
const Food = require("../model/Food");

class FoodRenderer extends Renderer {

    render(food, canvas) {
        if (!food instanceof Food) {
            throw new Error("Must be food");
        }

        canvas.drawSegment(food.position, this.config.food);
    }

}

module.exports = FoodRenderer;