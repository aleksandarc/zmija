"use strict"

const GameCanvas = require("./GameCanvas");
const Vector = require("../../model/Vector");

class WebGameCanvas extends GameCanvas {

    constructor(terrain) {
        super(terrain);
        this._canvas = document.getElementById('mycanvas');
        this._context = this._canvas.getContext("2d");
        this._segmentWidth = this._canvas.width / terrain.width;
        this._segmentHeight = this._canvas.height / terrain.height;
    }

    terrainToCanvas(position) {
        return new Vector(position.x * this._segmentWidth, position.y * this._segmentHeight);
    }

    drawSegment(position, options) {
        let transformedPosition = this.terrainToCanvas(position);
        this._context.fillStyle = options;
        this._context.fillRect(transformedPosition.x, transformedPosition.y, this._segmentWidth, this._segmentHeight);
    }

    clear() {
        this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
    }

}

module.exports = WebGameCanvas;
