"use strict";
const Player = require("../model/Player");

class PlayerGenerator {

    constructor(manager) {
        this._manager = manager;
    }

    /**
     * @return {PlayersManager}
     */
    get manager() {
        return this._manager;
    }

    generateInitialState(options) {
        this.manager.reset();
        options.forEach((opts) => {
            this.generate(opts);
        });
    }


    generate(opts) {
        return this.manager.add(new Player(opts));
    }

}

module.exports = PlayerGenerator;