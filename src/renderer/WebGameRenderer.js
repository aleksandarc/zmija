"use strict"

const GameRenderer = require("./GameRenderer");
const WebGameCanvas = require("../renderer/canvas/WebGameCanvas");

/**
 *Renderer renders the terrain and all gObjects inside of it.
 */
class WebGameRenderer extends GameRenderer {

    constructor(terrain, options) {
        super(terrain, options);
        this.canvas = new WebGameCanvas(terrain);
    }

    render(terrain) {
        for (let [k, gObject] of Object.entries(terrain.all)) {
            let renderer = this.loadRenderer(gObject);
            renderer.render(gObject, this.canvas);
        }
    }

    clearScreen() {
        if (this.canvas) {
            this.canvas.clear();
        }
        this._clearScore();
    }

    renderScore(player) {
        let scores = document.getElementById("scores");
        let node = document.createElement("LI");
        let textnode = document.createTextNode("Player " + player.name + ":" + player.score);
        node.appendChild(textnode);
        scores.appendChild(node);
    }

    _clearScore() {
        let scores = document.getElementById("scores");
        document.getElementById("scores").innerHTML = "";
    }

    renderMenu() {
        // No need to render anything as it is all visible in the HTML
    }

    renderPlayerInput(number, idContainer) {
        let container = document.getElementById(idContainer || "inputDivContainer");
        //cleaning existing DIV children
        while (container.hasChildNodes()) {
            container.removeChild(container.lastChild);
        }
        //Load desired number of input elements
        for (let i = 0; i < number; i++) {
            container.appendChild(document.createTextNode("Player " + (i + 1) + ":"));
            let input = document.createElement("input");
            input.type = "text";
            input.name = "inputName" + i;
            input.id = "inputId" + i;
            container.appendChild(input);
            container.appendChild(document.createElement("br"));
        }

    }


}


module.exports = WebGameRenderer;

