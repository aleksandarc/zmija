
function loadMap(){
    var fs = require('fs'),
    es = require('event-stream'),
    os = require('os');

var s = fs.createReadStream("map.txt")
    .pipe(es.split())
    .pipe(es.mapSync(function(line) {
        //pause the readstream
        s.pause();
        console.log("line:", line);
        s.resume();
    })
    .on('error', function(err) {
        console.log('Error:', err);
    })
    .on('end', function() {
        console.log('Finish reading.');
    })
);
    /*
    var index=0;
    for(var i=0;i<data.length;i++){
        world[index][i
    }
    */
    



    }



// Input (Config)
var TERRAIN_WIDTH = 30;
var TERRAIN_HEIGTH = 20;
var INIT_SNAKE_LENGTH = 2;
var INIT_FOOD_COUNT = 1;

var FPS = 2;
var FIELD_TYPE = [" ", "x", "o"];
var FIELD_TYPE_2 = {
    x: -1,
    space: 0,
    o: 1,
}

var DIRECTION = {
    N: { r: -1, c: 0 },
    S: { r: 1, c: 0 },
    E: { r: 0, c: 1 },
    W: { r: 0, c: -1 }
}
var ORIENTATION = Object.values(DIRECTION)

var KEY_MAP = {
    w: "N",
    s: "S",
    a: "W",
    d: "E",
}

function Particle(r, c) {
    this.r = r;
    this.c = c;
    this.stepR = (Math.floor(Math.random() * 5) + 1) * (Math.floor(Math.random() * 3) - 1);
    this.stepC = (Math.floor(Math.random() * 3) - 1) * (Math.floor(Math.random() * 3) - 1);
}

Particle.prototype.update = function () {
    console.log("Particle update");
    this.r += this.stepR;
    this.c += this.stepC;

}

function isParticle(r, c) {
    for (var i = 0; i < particles.length; i++) {
        if (particles[i].r == r && particles[i].c == c) {
            return true;
        }
    }
    return false;
}





var particles = [];

function generateParticles(r, c, n) {
    console.log("Particles generated");
    for (var i = 0; i < n; i++) {
        particles.push(new Particle(r, c));
    }

}



/********************************************************************************************************************/
//    Game Model
/********************************************************************************************************************/
/**
 * Creates a new game world.
 * 
 * @param {number} width 
 * @param {number} height 
 */
function generateWorld(width, height) {
    var world = [];
    for (var r = 0; r < height; r++) {
        var row = [];
        for (var c = 0; c < width; c++) {
            // TODO: Generalize this with a configuration parameter.
            if (r % 7 == 0 && c > 4 && c < width - 4){
                row.push({
                    pos: { r: r, c: c },
                    val: 1,
                });
            } else {
                row.push({
                    pos: { r: r, c: c },
                    val: 0,
                });
            }
        }
        world.push(row);
    }
    return world;
}

function addFoodToRandomLocation(world, snake) {
    while (true) {
        foodC = Math.floor(Math.random() * TERRAIN_WIDTH);
        foodR = Math.floor(Math.random() * TERRAIN_HEIGTH);

        if (isOccupied(world, snake, foodR, foodC)) {
            break;
        }
    }
    world[foodR][foodC].val = 2;
}

function isOccupied(world, snake, foodR, foodC) {
    return world[foodR][foodC].val == 0 && indexOfSnakeSegment(foodR, foodC, snake) < 0;
}

/**
 * Returns the center coordinates of the world
 * 
 * @param {*} world 
 * @return {{r:number, c:number}}
 */
function getCenterPosition(world) {
    return {
        r: Math.floor(world.length / 2),
        c: Math.floor(world[0].length / 2) + 1,
    };
}

/**
 * Returns a given world's random field coordinates 
 * @param {*} world 
 */
function getRandomPosition(world) {
    return {
        r: Math.floor((Math.random() * world.length)),
        c: Math.floor((Math.random() * world[0].length)),
    };
}

/**
 * Returns a random orientation in the form of a vector 
 * Ori   R,  C 
 * N - (-1,  0)
 * S - (+1,  0)
 * W - ( 0, -1)
 * E - ( 0, +1)
 * @returns {object}
 */
function getRandomOrientation() {
    return ORIENTATION[Math.floor(Math.random() * ORIENTATION.length)];
}

// function _isFieldAvailable(segment, snake) {
//     return true
//         && indexOfSnakeSegment(segment.r, segment.c, snake) < 0 // is the segment already in the snake
//         && true // TODO: Is the segment falling on a wall


// }

/**
 * Returns a memory snake model with a given number of segments, 
 * starting from the given position (where the head is located)
 * and having a given length number of segments
 * @param {*} length 
 * @param {*} headPosition 
 */
function generateSnake(length, headPosition, world) {
    if (length <= 0) {
        length = INIT_SNAKE_LENGTH;
    }
    if (!headPosition) {
        headPosition = { r: 0, c: 0 }
    }
    var snake = [headPosition]; // The first element is considered to be the head
    while (snake.length < length) {
        var orientation = getRandomOrientation();
        var segment = {
            r: snake[snake.length - 1].r + orientation.r,
            c: snake[snake.length - 1].c + orientation.c,
        };
        //console.log(snake.length);
        if (!checkCollision(snake, world)) {

            snake.push(segment);
        }
    }
    return snake;
}

/**
 * Returns the index of the snake segment that is located on the given coordinates.
 * 
 * @param {number} r The world row coordinate
 * @param {number} c The world column coordinate
 * @param {*} 
 */
function indexOfSnakeSegment(r, c, snake) {
    for (var i = 0; i < snake.length; i++) {
        if (snake[i].r === r && snake[i].c === c) {
            return i;
        }
    }
    return -1;
}

function checkCollision(snake, world) {
    // Check if the snake head had hit an obstacle
    if (0 < world[snake[0].r][snake[0].c].val) {
        return true;
    }
    // Check if the snake head had hit itself
    for (var i = 1; i < snake.length; i++) {
        if (snake[0].r == snake[i].r && snake[0].c == snake[i].c) {
            return true;
        }
    }
    return false;
}

/**
 * 
 * @param {*} world 
 * @param {*} snake 
 * @param {*} initalFoodCount 
 */

function generateFood(world, snake, initalFoodCount) {
    addFoodToRandomLocation(world, snake);
}

function update() {
    if (checkCollision(snake, world)) {
        return true;
    }


    // Move Snake
    if (step.r != step.c) {
        var segment = {
            r: snake[0].r + step.r,
            c: snake[0].c + step.c
        }
        // Go overboard
        if (segment.r < 0) {
            segment.r = TERRAIN_HEIGTH - 1;
        } else if (segment.c < 0) {
            segment.c = TERRAIN_WIDTH - 1;
        } else {
            segment.r %= world.length;
            segment.c %= world[0].length;
        }


        // TODO: Check if there is any movement at all
        snake.unshift(segment);

        if (_isFoodCollision(world, snake)) {
            _eatFood(world, snake);
            addFoodToRandomLocation(world, snake);
        } else {
            snake.pop();
        }
        return false;
    }

}

function _isFoodCollision(world, snake) {
    console.log(snake);
    return world[snake[0].r][snake[0].c].val == 2;
}

function _eatFood(world, snake) {
    world[snake[0].r][snake[0].c].val = 0;
    score++;
}


/********************************************************************************************************************/
//    Rendering the world
/********************************************************************************************************************/

/**
 * Encodes the field into an ASCII character based on the rules we have defined
 * @param {*} field 
 */

function encodeWorldField(field) {
    return FIELD_TYPE[field.val];
}

/**
 * Encodes the snake segment at the given index into an ASCII character based ont he rules we have defined
 * @param {*} snake 
 * @param {number} segmentIdx 
 */
function encodeSnakeSegment(snake, segmentIdx) {
    return segmentIdx ? "*" : "O";
}

/**
 * Renders the world and the snake to the console std out.
 * 
 * @param {*} world 
 * @param {*} snake 
 */
function draw(world, snake) {

    // Define a world visual edge
    var edge = "";
    for (var i = 0; i < TERRAIN_WIDTH; i++) {
        edge += String.fromCharCode(0x2500);
    }

    // Clear the world canvas
    console.clear();
    process.stdout.cursorTo(0);

    // Draw the top world visual edge
    console.log(String.fromCharCode(0x250C) + edge + String.fromCharCode(0x2510));

    // Draw the world
    for (var r = 0; r < world.length; r++) {
        var s = "";
        for (var c = 0; c < world[r].length; c++) {
            var segmentIdx = indexOfSnakeSegment(r, c, snake);
            //console.log(food);
            if (particles.length != 0 && isParticle(r, c)) {
                s += ".";
            }
            else if (0 <= segmentIdx) {
                s += encodeSnakeSegment(snake, segmentIdx);
            } else {
                s += encodeWorldField(world[r][c]);
            }
        }
        console.log(String.fromCharCode(0x2502) + s + String.fromCharCode(0x2502));
    }

    // Draw the bottom world visual edge
    console.log(String.fromCharCode(0x2514) + edge + String.fromCharCode(0x2518));
}

/********************************************************************************************************************/
//    User input management
/********************************************************************************************************************/

function initializeGameInputHandler() {
    const readline = require('readline');
    readline.emitKeypressEvents(process.stdin);
    process.stdin.setRawMode(true);
    var keys = Object.keys(KEY_MAP);
    process.stdin.on('keypress', (str, key) => {
        if (key && KEY_MAP[key.name]) {
            var orientation = DIRECTION[KEY_MAP[key.name]]
            step.c = orientation.c;
            step.r = orientation.r;
        } else if (key && key.ctrl && key.name == 'c') {// Control-C exits the game
            process.exit(0);
        }
    });
}

/********************************************************************************************************************/
//    Main game logic
/********************************************************************************************************************/

/*TODO:rethink this
while(true){
    console.log(-1%30);
}
*/


var world = generateWorld(TERRAIN_WIDTH, TERRAIN_HEIGTH);

var snake = generateSnake(600, getCenterPosition(world), world);
console.log("lalalal");
var foods = generateFood(world, snake, INIT_FOOD_COUNT);
var score = 0;
var step={r:0,c:0};
//var stepR = 0;
//var stepC = 0;

initializeGameInputHandler();

setInterval(function () {
    var error = update();
    if (error) {
        console.log("GAME OVER");
        if (particles.length == 0) {
            generateParticles(snake[0].r, snake[0].c, 100);
        }
        else {
            for (var i = 0; i < particles.length; i++) {
                particles[i].update();
            }
        }
        //process.exit(0);
    }
    draw(world, snake);
    console.log(score);
}, 1000 / FPS);

/**
 * Render the exposion when the Snake hits an obstacle
 * Separate the rendering thread from the model thread.
 */