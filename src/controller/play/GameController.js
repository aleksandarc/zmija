"use strict";

const PlayController = require("../PlayController");

class GameController extends PlayController {

    onRawKey(key) {
        super.onRawKey(key);
        if (key.name === 'p') {
            this.emit("pause");
        } else if (key.name === 'r') {
            this.emit("restart");
        } else if (key.name === 'q') {
            this.emit("quit");
        }
    }

    onLoadPlayerCount(playerCount) {
        this.emit("startMenuProcess", playerCount);
    }

    onLoadPlayerNames(players) {
        this.emit("completeMenuProcess", players);
    }

}

module.exports = GameController;