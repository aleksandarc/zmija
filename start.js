"use strict"

const config = require("./config/default");

const GameEngine = require("./src/GameEngine");

const gameEngine = new GameEngine(config, () => {
    process.exit(0);
});

/**
 * Run game engine
 */
gameEngine.boot();