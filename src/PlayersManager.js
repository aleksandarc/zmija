"use strict";

const Player = require("./model/Player");

class PlayersManager {

    constructor() {
        this._players = []
    }


    get players() {
        return this._players;
    }

    reset() {
        this._players = [];
    }

    getPlayerByHisGObject(gObject) {
        for (let i = 0; i < this._players.length; i++) {
            if (this._players[i].character.isEqual(gObject)) {
                return this._players[i];
            }
        }
        throw new Error("No player controls this GObject");
    }

    add(player) {
        if (!player instanceof Player) {
            throw new Error(`Invalid object type. Player expected: ${typeof player}`);
        }
        this._players.push(player);
        return player;
    }

}

module.exports = PlayersManager;