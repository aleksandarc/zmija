"use strict";

const PlayController = require("../PlayController");

class PlayerController extends PlayController {

    constructor(controls) {
        super();
        this.KEY_MAP = controls;
    }


    onRawKey(key) {
        if (this.KEY_MAP[key.name]) {
            this.emit("changeOrientation", this.KEY_MAP[key.name]);
        }
    }

}

module.exports = PlayerController;

