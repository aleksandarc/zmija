"use strict";

const FoodRenderer = require("../renderer/FoodRenderer");
const ObstacleRenderer = require("../renderer/ObstacleRenderer");
const SnakeRenderer = require("../renderer/SnakeRenderer");
const ParticleRenderer = require("../renderer/ParticleRenderer");

const Food = require("../model/Food");
const Obstacle = require("../model/Obstacle");
const Particle = require("../model/Particle");


/**
 *Renderer renders the terrain and all gObjects inside of it.
 */
class GameRenderer {

    static factory(isCLI, terrain, options) {
        let Renderer;
        let renderOptions;
        if (isCLI) {
            renderOptions = options.CLI;
            Renderer = require("./CliGameRenderer");
        } else {
            renderOptions = options.WEB;
            Renderer = require("./WebGameRenderer");
        }
        return new Renderer(terrain, renderOptions);
    }

    constructor(terrain, options) {
        this.foodRenderer = new FoodRenderer(options);
        this.obstacleRenderer = new ObstacleRenderer(options);
        this.snakeRenderer = new SnakeRenderer(options);
        this.particleRenderer = new ParticleRenderer(options);
    }

    /**
     * Loads renderer for specified GObject type
     * @param gObject
     * @returns {*}
     */
    loadRenderer(gObject) {
        if (gObject instanceof Food) {
            return this.foodRenderer;
        } else if (gObject instanceof Obstacle) {
            return this.obstacleRenderer;
        } else if (gObject instanceof Particle) {
            return this.particleRenderer;
        } else {
            return this.snakeRenderer;
        }
    }


    /**
     * Render GObjects on terrain
     * @param terrain
     */
    render(terrain) {

    }

    /**
     * Clear all GObject form screen
     */
    clearScreen() {

    }

    /**
     * Template method for render
     */
    renderWorld(terrain) {
        this.clearScreen();
        this.render(terrain);
    }

    /**
     * Render score
     * @param player - render score for this player
     */
    renderScore(player) {
        // TODO: Temporary in order to test
        let old;
        if (cache.hasOwnProperty(player.name)) {
            old = cache[player.name]
        } else {
            cache[player.name] = player.score;
        }
        if (old !== player.score) {
            console.log(player.name, player.score);
        }
        // throw new Error("Must be implemented");
    }

    /**
     * Render gui elements(for example score)
     * @param manager
     */
    renderGUI(manager) {
        manager.players.forEach((player) => {
            this.renderScore(player);
        })
    }


    /**
     * Renders the Main menu
     */
    renderMenu() {
        // Here we can Clear the already rendered Menu
        // and render the menu if necessary

    }

}

const cache = {};

module.exports = GameRenderer;

