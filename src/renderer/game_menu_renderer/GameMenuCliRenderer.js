"use strict";

const GameMenuRenderer = require("./GameMenuRenderer");

class GameMenuCliRenderer extends GameMenuRenderer {

    renderImputPlayerCount() {
        console.log("-----------------------");
        console.log("Insert the number of players");
        console.log("-----------------------");
    }

    renderInputPlayer(playerNumber) {
        console.log("-----------------------");
        console.log(`Insert ${playerNumber}. player`);
        console.log("-----------------------");
    }

}

module.exports = GameMenuCliRenderer;