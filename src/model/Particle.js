"use strict";

const GObject = require("./GObject");

/**
 * Particle is small GObject with random orientation and speed.
 * Sets of particles can be used for simulating explosions and
 * other effects.
 */
class Particle extends GObject {

    constructor(position, orientation, maxSpeed) {
        super(position);
        this.orientation = orientation;
        this.maxSpeed = maxSpeed;
    }

    get movable() {
        return true;
    }

    get colidable() {
        return false;
    }

    get endGameActive() {
        return true;
    }

    get speed() {
        return Math.floor(Math.random() * this.maxSpeed) + 1;
    }

    //Move particle
    move(terrain) {
        this.position = terrain.projectVectorToTerrain(this.position.addVector(this.orientation.multiplyWithScalar(this.speed)));
    }

}


module.exports = Particle;