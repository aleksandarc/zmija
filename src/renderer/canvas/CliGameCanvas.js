"use strict"

const GameCanvas = require("./GameCanvas");

class CliGameCanvas extends GameCanvas {

    constructor(terrain) {
        super(terrain);
        this.matrixForDraw = [];
        this._initMatrixForDraw();

    }

    _initMatrixForDraw() {
        this.matrixForDraw = [];
        for (let i = 0; i < this.height; i++) {
            let row = [];
            for (let j = 0; j < this.width; j++) {
                row[j] = " ";
            }

            this.matrixForDraw.push(row);
        }
    }

    canvasToStr() {
        let output = this._drawTopEdge(this.width) + "\n";

        for (let i = 0; i < this.height; i++) {
            output += "|";
            for (let j = 0; j < this.width; j++) {
                output += this.matrixForDraw[i][j];
            }
            output += "|\n";
        }

        output += this._drawBottomEdge(this.width);
        return output;
    }


    drawSegment(position, options) {
        return this.matrixForDraw[position.y][position.x] = options;
    }

    clear() {
        this._initMatrixForDraw();
    }


    _drawTopEdge(width) {
        return String.fromCharCode(0x250C) + this._edge(width) + String.fromCharCode(0x2510);
    }

    _drawBottomEdge(width) {
        return String.fromCharCode(0x2514) + this._edge(width) + String.fromCharCode(0x2518);
    }

    _edge(width) {
        let edge = "";
        for (let i = 0; i < width; i++) {
            edge += String.fromCharCode(0x2500);
        }
        return edge;
    }
}

module.exports = CliGameCanvas;