"use strict"

class Vector {

    constructor(x, y) {
        if (x instanceof Object) {
            y = x.y;
            x = x.x;
        }
        this.x = x;
        this.y = y;
    }


    /**
     *
     * @returns {Vector} with x=0 and y=0
     */
    static get Zero() {
        return ZERO_VECTOR;
    }

    static get ORTOGONAL_DIRECTIONS() {
        return ORTOGONAL_DIRECTIONS;
    }

    static get DIAGONAL_DIRECTIONS() {
        return DIAGONAL_DIRECTIONS;
    }

    static get ORTOGONAL_ORIENTATIONS() {
        return ORTOGONAL_ORIENTATIONS;
    }

    static get DIAGONAL_ORIENTATIONS() {
        return DIAGONAL_ORIENTATIONS;
    }

    static get ORIENTATIONS() {
        return ORIENTATIONS;
    }

    /**
     * Should use las vegas algoritm or using standard algoritm
     * @param exludedNumber
     * @param availablesNumber
     */
    static lasVegasTrashhold(exludedNumber, availablesNumber) {
        return exludedNumber <= availablesNumber / 2;
    }


    /**
     * @param args excluded directions
     * @returns random orientation
     */
    static randomOrientation(excluded, available) {
        let availables = available || ORTOGONAL_ORIENTATIONS;
        if (this.lasVegasTrashhold(excluded.length, availables.length)) {
            return this.randomOrientationLasVegas(excluded, availables);
        }
        let difference = this.calculateSetsDifference(availables, excluded);
        return difference[Math.floor(Math.random() * difference.length)];
    }

    static randomOrientationLasVegas(excluded, availables) {
        while (true) {
            let orientation = availables[Math.floor(Math.random() * availables.length)];
            if (!orientation.isVectorIncluded(excluded)) {
                return orientation;
            }
        }
    }

    /**
     *
     * @param setA first set
     * @param setB second set
     * @returns {Array} difference of setA and setB
     */
    static calculateSetsDifference(setA, setB) {
        let difference = [];
        for (let i = 0; i < setA.length; i++) {
            let addToDiference = true;
            for (let j = 0; j < setB.length; j++) {
                if (setA[i].isEqual(setB[j])) {
                    addToDiference = false;
                    break;
                }
            }
            if (addToDiference == true) {
                difference.push(setA[i]);
            }
        }
        return difference;
    }

    /**
     * Checks is vector included in set of vectors
     * @param set set of vectors
     * @returns {boolean}
     */
    isVectorIncluded(set) {
        for (let i = 0; i < set.length; i++) {
            if (set[i].isEqual(this)) {
                return true;
            }
        }
        return false;
    }

    static getDirectionVector(direction) {
        if (!direction) {
            direction = ORTOGONAL_ORIENTATIONS[Math.floor(Math.random(ORTOGONAL_ORIENTATIONS.length))]
        }
        if (!ORTOGONAL_DIRECTIONS.hasOwnProperty(direction)) {
            throw new Error(`Unsupported direction: ${direction}`);
        }
        return ORTOGONAL_DIRECTIONS[direction];
    }

    addVector(other) {
        if (!(other instanceof Vector)) {
            throw new Error("Not a vector");
        }
        let x = this.x + other.x;
        let y = this.y + other.y;
        return new Vector(x, y);
    }

    substractVector(other) {
        return this.addVector(other.negate());
    }

    multiplyWithScalar(scalar) {
        if (!Number.isInteger(scalar)) {
            throw new Error("Not a scalar");
        }
        let x = this.x * scalar;
        let y = this.y * scalar;
        return new Vector(x, y);
    }

    isEqual(other) {
        if (!(other instanceof Vector)) {
            throw new Error("Not a vector");
        }
        return this.x == other.x && this.y == other.y;
    }

    negate() {
        return this.multiplyWithScalar(-1);
    }

}

/**
 * Directions for diagonal movement
 * @type {{NW: Vector, NE: Vector, SW: Vector, SE: Vector}}
 */
const DIAGONAL_DIRECTIONS = {
    NW: new Vector({y: -1, x: -1}),
    NE: new Vector({y: -1, x: 1}),
    SW: new Vector({y: 1, x: -1}),
    SE: new Vector({y: 1, x: 1})
};

const ORTOGONAL_DIRECTIONS = {
    N: new Vector({y: -1, x: 0}),
    S: new Vector({y: 1, x: 0}),
    E: new Vector({y: 0, x: 1}),
    W: new Vector({y: 0, x: -1}),
};


const ORTOGONAL_ORIENTATIONS = Object.values(ORTOGONAL_DIRECTIONS);
const DIAGONAL_ORIENTATIONS = Object.values(DIAGONAL_DIRECTIONS);
const ORIENTATIONS = DIAGONAL_ORIENTATIONS.concat(ORTOGONAL_ORIENTATIONS);
const ZERO_VECTOR = new Vector(0, 0);

module.exports = Vector;