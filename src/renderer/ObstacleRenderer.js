"use strict";

const Renderer = require("./Renderer");
const Obstacle = require("../model/Obstacle");

class ObstacleRenderer extends Renderer {

  render(obstacle, canvas) {
    if(!(obstacle instanceof Obstacle)) {
      throw new Error("Must be obstacle");
    }
    canvas.drawSegment(obstacle.position, this.config.obstacle);
  }

}

module.exports = ObstacleRenderer;