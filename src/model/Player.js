"use strict";

const GObject = require("./GObject");

class Player {

    constructor(opts) {
        this.name = opts.name;
        this._gObject = undefined;
        this._score = 0;
    }

    get score() {
        return this._score;
    }

    resetScore() {
        this._score = 0;
    }

    scoreIncrement() {
        this._score += 10;
    }

    get character() {
        return this._gObject;
    }

    set character(gObject) {
        if (!gObject instanceof GObject) {
            throw new TypeError(`GObject expected: ${typeof gObject}`);
        }
        if (!gObject.controllable) {
            throw new TypeError(`Controllable GObject expected`);
        }
        this._gObject = gObject;
    }


}

module.exports = Player;