"use strict";

const InputController = require("../InputController");

class DOMController extends InputController {


    _enableInGameControls() {
        // TODO: Checkout WTF?
        // let idGameContainer = "mycanvas";
        // document.getElementById(idGameContainer).addEventListener("keydown", (event) => {
        document.addEventListener("keydown", (event) => {
            // We normalize the events the best we can
            this._handle({
                sequence: event.key,
                name: event.key,
                ctrl: event.ctrlKey,
                meta: event.metaKey,
                shift: event.shiftKey,
            });
        }, false);
    }

    startMenu() {
        let idPlayerNumber = "inputPlayerNumber";
        // TODO: Maybe let idMainMenu= "loadInputs";
        document.getElementById("loadInputs").addEventListener("click", () => {
            // TODO@leo: Here we can add some validation
            let count = document.getElementById(idPlayerNumber).value;
            if (count <= 0) {
                // document.getElementById("inputPlayerNumber").style = "bg-color: red"
                return
            }
            if (2 < count) {
                // document.getElementById("inputPlayerNumber").style = "bg-color: red"
                return
            }
            this._emitPlayerCount(count);
        });

        let idContainer = "inputDivContainer";
        document.getElementById("loadPlayerNames").addEventListener("click", () => {
            let players = [];
            let inputs = document.querySelectorAll("#" + idContainer + " > input");
            inputs.forEach((el) => {
                players.push({
                    name: el.value,
                });
            });
            this.emit("loadPlayerNames", players);
            this._enableInGameControls();
        });

    }

}

module.exports = DOMController;