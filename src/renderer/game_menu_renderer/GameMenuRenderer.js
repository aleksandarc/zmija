"use strict";

/**
 * This class renders game menu
 */
class GameMenuRenderer {


    /**
     * Render menu for input number of players
     */
    renderInputPlayerCount() {

    }

    /**
     * Render menu for input player name
     * @param playerNumber number of player
     */
    renderInputPlayer(playerNumber) {

    }

}

module.exports = GameMenuRenderer;