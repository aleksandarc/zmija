"use strict";

const async = require("async");
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const InputController = require("../InputController");

class CLIController extends InputController {


    _enableInGameControls() {
        readline.emitKeypressEvents(process.stdin);
        if (process.stdin.setRawMode) {
            process.stdin.setRawMode(true);
        }
        process.stdin.on("keypress", (str, key) => {
            this._handle(key);
        });
    }


    startMenu() {
        this.inputNumberOfPlayers((err, count) => {
            if (err) {
                return this.emit("error", err);
            }
            this.inputPlayers(count, (err, users) => {
                if (err) {
                    this.emit("error", err);
                } else {
                    this.emit("loadPlayerNames", users);
                    this._enableInGameControls();
                }
            });
        });
    }

    inputNumberOfPlayers(done) {
        rl.question("Insert number of players", function (count) {
            console.log()
            if (!(Number.isInteger(Number(count)) && 0 < count && count <= 2)) {
                done("Invalid number of players");
            }
            done(undefined, count);
        }.bind(this));
    }

    inputPlayers(count, done) {
        async.timesSeries(count, function (n, next) {
            rl.question("Insert user", (name) => {
                let user = {};
                user.name = name;
                next(undefined, user);
            });
        }, done);


    }

}

module.exports = CLIController;