"use strict";

const events = require("events");

class InputController extends events.EventEmitter {

    static factory(isCLI, options) {
        let Controller;
        if (isCLI) {
            Controller = require("./input/CLIController");
        } else {
            Controller = require("./input/DOMController");
        }
        return new Controller(options);
    }

    /**
     * This method starts listening for in game controls
     * @private
     */
    _enableInGameControls() {

    }

    _handle(key) {
        this.emit("raw_key", key);
    }

    _emitPlayerCount(playerCount) {
        this.emit("loadPlayerCount", playerCount);
    }

    _emitPlayerCount(playerCount) {
        this.emit("loadPlayerCount", playerCount);
    }

    /**
     * Initialize game menu,get player names and then start game
     */
    startMenu() {

    }
}

module.exports = InputController;