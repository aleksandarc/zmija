"use strict";


const events = require("events");

/**
 *GObject is an abstraction of a game object that resides in the game world
 */


let idCounter = 0;

class GObject extends events.EventEmitter {

    constructor(position, id) {
        super();
        this.id = id || idCounter++;
        this.position = position;
    }


    get movable() {
        return false;
    }

    get colidable() {
        return true;
    }

    get controllable() {
        return false;
    }

    /**
     * Some GObjects can be active after game ends(animations,particles...).
     * @returns is object active after game ends.
     */
    get endGameActive() {
        return false;
    }

    isEqual(gObject) {
        return this.id === gObject.id;
    }


    isOccupied(vector) {
        return this.position.isEqual(vector);
    }

    /**
     * Check collision with other game object.
     *
     * @param {GObject} gObject Another game object
     * @returns {boolean}
     */
    hasCollided(gObject) {
        if (this.isEqual(gObject)) {
            return false;
        }
        return this.isOccupied(gObject.position) || gObject.isOccupied(this.position);
    }

    emitCollision(gObject) {
        this.emit("collision", {from: this, with: gObject, at: 0});
    }

}

module.exports = GObject;