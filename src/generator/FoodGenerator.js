"use strict";

const GObjectGenerator = require("./GObjectGenerator");
const Food = require("../model/Food");

class FoodGenerator extends GObjectGenerator {

    generateInitialState() {
        for (let i = 0; i < this.options.initial_count; i++) {
            this.generate(this.options);
        }
    }

    generate(opts) {
        return this.terrain.add(new Food(this.terrain.getRandomPosition(true)));
    }


}

module.exports = FoodGenerator;