"use strict";

const GObjectGenerator = require("./GObjectGenerator");
const Vector = require("../model/Vector");
const Particle = require("../model/Particle");

class ParticleGenerator extends GObjectGenerator {

    /**
     * Generate set of particle on one location.
     * @param position where we generate particles
     */
    generateParticles(position) {
        this.options.position = position;
        for (let i = 0; i < this.options.initial_count; i++) {
            this.generate(this.options);
        }
    }

    generate(opts) {
        let particle = new Particle(opts.position, Vector.randomOrientation([], Vector.ORIENTATIONS), opts.max_speed);
        this.terrain.add(particle);
        return particle;
    }

}

module.exports = ParticleGenerator;