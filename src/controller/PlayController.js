"use strict";

const events = require("events");

class PlayController extends events.EventEmitter {

    onRawKey(key) {
        if (key.ctrl && key === 'c') {// Control-C exits the game
            this.emit("quit");
        }
    }
}

module.exports = PlayController;