"use strict";

const Renderer = require("./Renderer");
const Snake = require("../model/Snake");

class SnakeRenderer extends Renderer {

    render(snake, canvas) {
        if (!(snake instanceof Snake)) {
            throw new Error("Must be snake");
        }
        snake.segments.forEach((segment) => {
            canvas.drawSegment(segment, this.config.snake.body);
        });
        canvas.drawSegment(snake.head, this.config.snake.head);
    }

}

module.exports = SnakeRenderer;