# GinkoSnake

<!--TODO: Finish this -->

## Overview
This is a modern take on a historical game we all know and love.

## Prerequisites
* What we need to even start working on this project:
    *    Node >= v8.0
    *    JavaScript capable internet browser (Chrome, Mozilla...)


## Installation
```bash
npm install 
```

## Running
###CLI Mode
```bash
npm start
```

### WEB Mode

* index.html
```html
<html>
<head>
    <title>Socket.IO chat</title>
</head>
<body>
<canvas id="mycanvas" width="600" height="400" style="border:1px solid #000000;"></canvas>
<ul id="scores">SCORES: </ul>
<script src="./dist/bundled.js"></script>
</body>
</html>
```



## Testing
```bash
npm test
```

## Documentation
* [Project Documentation](./doc/toc.md)
* [Business Requirement Documentation](./doc/BRD.md)


## Contributors
* Aleksandar Ostojić - @eon
* Aleksandar Čoha - @pow
* Mihailo Radović - @com
* Leon Gradišar - @leo
* Vladimir Radmanović - @cap
* Jelena Dragović - @wiz

### Contributing
 


