"use strict"

const GObject = require("../model/GObject");

class GObjectStorage {

    constructor() {
        this._gObjects = {};
        this._movable = {};
        this._collidable = {};
        this._controllable = {};
        this._endGameActive = {};
    }


    get gObjects() {
        return this._gObjects;
    }

    get movable() {
        return this._movable;
    }

    get collidable() {
        return this._collidable;
    }

    get controllable() {
        return this._controllable;
    }

    get endGameActive() {
        return this._endGameActive;
    }

    add(gObject) {
        if (!gObject instanceof GObject) {
            throw new Error(`Invalid object type. GObject expected: ${typeof gObject}`);
        }
        this._gObjects[gObject.id] = gObject;
        if (gObject.movable) {
            this._movable[gObject.id] = gObject;
        }
        if (gObject.colidable) {
            this._collidable[gObject.id] = gObject;
        }
        if (gObject.controllable) {
            this._controllable[gObject.id] = gObject;
        }
        if (gObject.endGameActive) {
            this._endGameActive[gObject.id] = gObject;
        }
        return gObject;
    }


    /**
     * Removes a gObject from the terrain
     * @param {GObject} gObject
     * @returns {GObject|undefined}
     */
    remove(gObject) {
        if (!gObject instanceof GObject) {
            throw new Error(`Invalid object type. GObject expected: ${typeof gObject}`);
        }
        return this.removeByID(gObject.id);

    }

    /**
     * Removes a gObject from the terrain by it's ID
     * @param {string} id
     * @returns {GObject|undefined}
     */
    removeByID(id) {
        if (!this._gObjects.hasOwnProperty(id)) {
            return false;
        }
        if (this._gObjects.hasOwnProperty(id)) {
            let gObject = this._gObjects[id];
            delete this._gObjects[id];
            delete this._movable[id];
            delete this._collidable[id];
            delete this._controllable[id];
            delete this._endGameActive[id];
            return gObject;
        }
        return undefined;
    }

    /**
     * Removes all GObject from the terrain
     */
    clear() {
        this._gObjects = {};
        this._movable = {};
        this._collidable = {};
        this._controllable = {};
        this._endGameActive = {};
    }


    isOccupied(vector) {
        for (let id in this.collidable) {
            if (this.collidable[id].isOccupied(vector)) {
                return true;
            }
        }
        return false;
    }

    hasCollided(gObject) {
        throw new Error(`Not Implemented: GObjectStorage.hasCollided`)
        // make sure that the gObject is not already contained within the world.
        // TODO@wiz: research if used and/or needed;
    }

}

module.exports = GObjectStorage;