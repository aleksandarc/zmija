"use strict";

const Snake = require("../model/Snake");
const Vector = require("../model/Vector");
const GObjectGenerator = require("./GObjectGenerator");

class SnakeGenerator extends GObjectGenerator {

    generateInitialState() {
        this.options.forEach((opts) => this.generate(opts));
    }

    generate(opts) {
        let snake = new Snake(new Vector(opts.start_position));
        while (snake.size < opts.start_length) {
            let excludedOrientations = [];
            while (excludedOrientations.length !== Vector.ORIENTATIONS.length) {
                let segment = this.terrain.getRandomNeighbour(snake.head, excludedOrientations);
                if (!this.terrain.isOccupied(segment)) {
                    snake.extendHead(segment);
                    break;
                }
                excludedOrientations.push(segment.substractVector(snake.head));
            }
            if (excludedOrientations.length === Vector.ORIENTATIONS.length) {
                throw new Error("Snake cant be generated");
            }
        }
        return this.terrain.add(snake);
    }

}

module.exports = SnakeGenerator;