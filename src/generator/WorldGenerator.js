"use strict";

const FoodGenerator = require("../generator/FoodGenerator");
const SnakeGenerator = require("../generator/SnakeGenerator");
const ObstacleGenerator = require("../generator/ObstacleGenerator");
const ParticleGenerator = require("../generator/ParticleGenerator");

class WorldGenerator {

    constructor(terrain, options) {
        this._terrain = terrain;
        this._obastacleGenerator = new ObstacleGenerator(terrain, options.Obstacles);
        this._foodGenerator = new FoodGenerator(terrain, options.Food);
        this._particleGenerator = new ParticleGenerator(terrain, options.Particle);
        this._snakeGenerator = new SnakeGenerator(terrain, options.Snake);
    }


    /**
     * Generate particles.This method is separate from other
     * GObjects generating because we want particles on game
     * end.
     * @param position where generate particles
     */
    generateParticles(position) {
        this._particleGenerator.generateParticles(position);
    }

    generateInitialState() {
        this._terrain.clear();
        this._obastacleGenerator.generateInitialState();
        this._foodGenerator.generateInitialState();
    }

    generateCharacter(opts) {
        return this._snakeGenerator.generate(opts);
    }

}

module.exports = WorldGenerator;