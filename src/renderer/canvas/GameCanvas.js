"use strict"

const terrain = require("../../model/Terrain");

class GameCanvas {

    constructor(terrain) {
        this.width = terrain.width;
        this.height = terrain.height;
    }

    drawSegment(position, options) {
        throw new Error('Needs to be implemented');
    }

    clear() {
        throw new Error('Needs to be implemented');
    }

}

module.exports = GameCanvas;